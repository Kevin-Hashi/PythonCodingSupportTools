package jp.gr.java_conf.hashiguchi_homemade.pycodingsupporter

import androidx.compose.foundation.layout.height
import androidx.compose.foundation.shape.CornerBasedShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.sp
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

data class CodePartData(
    val code: String,
    val fontSize: TextUnit,
    var modifier: Modifier,
    val cornerShape: CornerBasedShape
)
class CodePart(
    code: String,
    fontSize: TextUnit = 15.sp,
    height: Dp = 36.dp,
    cornerShape: CornerBasedShape = RoundedCornerShape(height)
){
    private val modifier=Modifier.height(height)
    private val data = CodePartData(code, fontSize, modifier,cornerShape)
    fun getCode() = data.code
    fun getFontSize() = data.fontSize
    fun getModifier() = data.modifier
    fun setModifier(modifier: Modifier) {
        data.modifier = modifier
    }
    fun getShape() = data.cornerShape
}