package jp.gr.java_conf.hashiguchi_homemade.pycodingsupporter

import androidx.compose.foundation.border
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp


class MySnippet(vararg snippet: CodePart){
    private val snippets= mutableListOf(*snippet)
    fun add(vararg snippet: CodePart){
        snippets.addAll(snippet)
    }
    fun remove(vararg snippet: CodePart){
        snippets.removeAll(snippet.toList())
    }
    fun removeAt(index: Int){
        snippets.removeAt(index)
    }
    @Composable
    fun Display(){
        Column(
            Modifier
                .fillMaxWidth()
                .padding(5.dp)
                .border(width = 2.dp, color = MaterialTheme.colors.onBackground)){
            Spacer(modifier = Modifier.height(15.dp))
            Row(modifier = Modifier
                .fillMaxWidth()
                .horizontalScroll(rememberScrollState())){
                Spacer(modifier =Modifier.width(5.dp))
                for(snippet in snippets){
                    Snippet(codePart = snippet)
                    Spacer(modifier = Modifier.width(10.dp))
                }
            }
            Spacer(modifier =Modifier.height(15.dp))
        }
    }
}