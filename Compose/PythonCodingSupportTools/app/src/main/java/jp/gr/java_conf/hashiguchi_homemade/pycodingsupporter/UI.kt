package jp.gr.java_conf.hashiguchi_homemade.pycodingsupporter

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import jp.gr.java_conf.hashiguchi_homemade.pycodingsupporter.ui.theme.PyCodingSupporterTheme

@Composable
fun Display(){
    Column(modifier= Modifier
        .fillMaxSize()
        .padding(20.dp)){
        Spacer(modifier = Modifier.height(30.dp))
        DisplaySnippets()
        Spacer(modifier = Modifier.height(30.dp))
        DisplayCodeEditor()
    }
}

@Composable
fun DisplaySnippets(){
    val mySnippet= MySnippet()
    mySnippet.add(CodePart("print('Hello, Android!')"),CodePart("import *"), CodePart("import **"))
    mySnippet.Display()
}
@Composable
fun DisplayCodeEditor(){
    Column(modifier = Modifier
        .fillMaxWidth()
        .fillMaxHeight()
        .padding(start = 5.dp, end = 5.dp, top = 0.dp, bottom = 30.dp)
        .border(width = 2.dp, color = MaterialTheme.colors.onBackground)){

        /*TODO*/

    }
}

@Composable
fun Snippet(codePart: CodePart){
    Button(
        onClick = {/*TODO*/},
        modifier = codePart.getModifier(),
        shape = codePart.getShape(),
    ) {
        Text(text=codePart.getCode(), fontSize = codePart.getFontSize())
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    PyCodingSupporterTheme {
        Display()
    }
}