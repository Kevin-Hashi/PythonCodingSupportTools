package jp.gr.java_conf.hashiguchi_homemade.pythoncodingsupporttool

import android.content.Context
import android.graphics.Typeface
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter

class FontSelector(private val context: Context) {
    private var font = "fonts/JetBrainsMono-VariableFont_wght.ttf"
    private val assets = context.assets
    private val rawFonts = if (assets.list("fonts") == null) {
        listOf("")
    } else {
        assets.list("fonts")?.toList()
    }
    private var fonts: MutableList<String> = rawFonts?.toMutableList() ?: mutableListOf("")

    init {
        for (i in fonts.indices) {
            fonts[i] = fonts[i].shortFontName()
        }
    }

    val adapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, fonts)
    val listener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            val fontName = adapter.getItem(position)
            font = getMatchFont(fontName.toString())
            setSharedPreferences(font)
            Log.i("FontSelector:Spinner", font)
        }
    }


    fun getTypeface(): Typeface? {
        font = "fonts/" + getMatchFont(getSharedPreferences())
        Log.i("FontSelector", font)
        return Typeface.createFromAsset(context.assets, font)
    }

    fun getTypefaceAsString(): String {
        font = "fonts/" + getMatchFont(getSharedPreferences())
        return font
    }

    fun getSelectedIndex(): Int {
        for (i in fonts.indices) {
            if (fonts[i] == getTypefaceAsString().shortFontName()) {
                Log.i("FontSelector", "index: $i")
                return i
            }
        }
        for (i in fonts.indices){
            if (fonts[i] == "JetBrainsMono") {
                Log.i("FontSelector", "JetBrains Mono: $i")
                return i
            }
        }
        Log.i("FontSelector", "index: force 0")
        return 0
    }

    private fun String.shortFontName(): String {
        return this.replace("fonts/", "").replace(".ttf", "").replace("""-.*""".toRegex(), "")
    }

    private fun getMatchFont(font: String): String {
        for (i in rawFonts?.indices!!) {
            if (rawFonts[i].startsWith(font)) {
                return rawFonts[i]
            }
        }
        return "JetBrainsMono-VariableFont_wght.ttf"
    }

    private fun setSharedPreferences(fontName: String) {
        val sharedPreferences = context.getSharedPreferences("font", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("font", fontName)
        editor.apply()
    }

    private fun getSharedPreferences(): String {
        val sharedPreferences = context.getSharedPreferences("font", Context.MODE_PRIVATE)
        return sharedPreferences.getString("font", "fonts/JetBrainsMono-VariableFont_wght.ttf")
            .toString()
    }
}