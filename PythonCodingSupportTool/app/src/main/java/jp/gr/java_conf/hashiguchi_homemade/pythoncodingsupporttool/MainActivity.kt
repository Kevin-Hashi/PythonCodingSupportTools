package jp.gr.java_conf.hashiguchi_homemade.pythoncodingsupporttool

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.AppLaunchChecker
import jp.gr.java_conf.hashiguchi_homemade.pythoncodingsupporttool.databinding.ActivityMainBinding
import kotlin.math.max
import kotlin.math.min

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var snippetManager: SnippetManager
    private lateinit var fontSelector: FontSelector
    override fun onCreate(savedInstanceState: Bundle?) {
        Log.i("MainActivity", "onCreate")
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        fontSelector = FontSelector(this)
        snippetManager = SnippetManager(this)
        if (!AppLaunchChecker.hasStartedFromLauncher(this)) {
            Log.d("MainActivity", "hasStartedFromLauncher")
            settingIntent()
        }
        binding.settingButton.setOnClickListener {
            settingIntent()
        }

        AppLaunchChecker.onActivityCreate(this)
    }

    override fun onResume() {
        super.onResume()
        Log.i("MainActivity", "onResume")

        binding.snippetContainer.removeAllViews()
        binding.editCode.typeface = fontSelector.getTypeface()
        Log.i("MainActivity", "onResume typeface = ${fontSelector.getTypefaceAsString()}")
        Log.i("MainActivity", "onResume typeface = ${binding.editCode.typeface}")
        snippetManager.getSnippetList().forEach {
            binding.snippetContainer.addView(it.getButton())
            it.getButton().setOnClickListener { _ ->
                Log.i("MainActivity", "button clicked:${it.code}")
                val start: Int = binding.editCode.selectionStart
                val end: Int = binding.editCode.selectionEnd
                if (start ==end &&start ==-1) {
                    binding.editCode.text.also { text ->
                        text.insert(text.length, text.toString()+it.code)
                    }
                } else {
                    binding.editCode.text.replace(min(start, end), max(start, end), it.code)
                }
            }
        }
    }

    private fun settingIntent() {
        val intent = Intent(application, SettingSnippet::class.java)
        startActivity(intent)
    }
}