package jp.gr.java_conf.hashiguchi_homemade.pythoncodingsupporttool

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageButton
import androidx.core.content.ContextCompat
import jp.gr.java_conf.hashiguchi_homemade.pythoncodingsupporttool.databinding.SnippetSettingBinding

class SettingSnippet : AppCompatActivity() {
    private lateinit var binding: SnippetSettingBinding
    private lateinit var snippetManager: SnippetManager
    private lateinit var fontSelector: FontSelector
    private lateinit var dialogLayout: View
    private lateinit var editSnippet: AppCompatEditText
    override fun onCreate(savedInstanceState: Bundle?) {
        Log.i("SettingSnippet", "onCreate")
        super.onCreate(savedInstanceState)
        binding = SnippetSettingBinding.inflate(layoutInflater)
        setContentView(binding.root)
        snippetManager = SnippetManager(this, ::addSnippetLayout)
        fontSelector = FontSelector(this)
        binding.fontSpinner.adapter = fontSelector.adapter
        binding.fontSpinner.onItemSelectedListener = fontSelector.listener
        binding.fontSpinner.setSelection(fontSelector.getSelectedIndex())
    }

    override fun onResume() {
        Log.i("SettingSnippet", "onResume")
        super.onResume()
        Log.i("SettingSnippet:onResume", snippetManager.getSnippetListAsStringList().toString())
        binding.subAddButton.setOnClickListener {
            dialogLayout =
                LayoutInflater.from(this).inflate(R.layout.layout_edit_snippet_dialog, null)
            editSnippet = dialogLayout.findViewById(R.id.editSnippetDialog)
            editSnippet.typeface = fontSelector.getTypeface()

            val dialog = AlertDialog.Builder(this)
                .setTitle("スニペットを追加")
                .setView(dialogLayout)
                .setPositiveButton("OK") { dialog, _ ->
                    val snippet = editSnippet.text.toString()
                    snippetManager.add(snippet)
                    Log.i(
                        "SettingSnippet:onCreate",
                        snippetManager.getSnippetListAsStringList().toString()
                    )
                    dialog.dismiss()
                }
                .setNegativeButton("キャンセル") { dialog, _ ->
                    dialog.dismiss()
                }
                .create()
            dialog.show()
            dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                .setTextColor(ContextCompat.getColor(this, R.color.gray))
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
                .setTextColor(ContextCompat.getColor(this, R.color.purple_350))
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = false
            editSnippet.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun afterTextChanged(p0: Editable?) {
                    Log.i("afterTextChanged", (!p0.isNullOrEmpty()).toString())
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = !p0.isNullOrEmpty()
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(
                        ContextCompat.getColor(
                            this@SettingSnippet,
                            if (p0.isNullOrEmpty()) R.color.gray else R.color.purple_350
                        )
                    )
                }
            })
        }
        removeAllSnippetLayout()
        snippetManager.getSnippetList().forEach {
            addSnippetLayout(it.code)
        }
    }

    private fun addSnippetLayout(snippet: String) {
        val snippetLayout = layoutInflater.inflate(R.layout.snippet_list, binding.subLinear0, false)
        snippetLayout.findViewById<TextView>(R.id.snippet_list_code).apply {
            text = snippet
            typeface = fontSelector.getTypeface()
        }
        snippetLayout.findViewById<AppCompatImageButton>(R.id.snippet_delete).setOnClickListener {
            for (i in 0 until binding.subLinear0.childCount) {
                if (binding.subLinear0.getChildAt(i) == snippetLayout) {
                    snippetManager.removeAt(i)
                    break
                }
            }
            binding.subLinear0.removeView(snippetLayout)
        }
        binding.subLinear0.addView(snippetLayout)
    }

    private fun removeAllSnippetLayout() {
        binding.subLinear0.removeAllViews()
        Log.i("SettingSnippet", "removeAllSnippetLayout")
    }
}