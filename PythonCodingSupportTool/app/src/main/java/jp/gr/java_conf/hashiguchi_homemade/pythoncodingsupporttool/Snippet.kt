package jp.gr.java_conf.hashiguchi_homemade.pythoncodingsupporttool

import android.content.Context
import android.view.Gravity
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import androidx.core.view.setMargins

class Snippet(context: Context, val code: String) {
    private val fontSelector = FontSelector(context)
    private val button = Button(context).apply {
        text = code
        val layoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        layoutParams.gravity = Gravity.CENTER_VERTICAL
        layoutParams.setMargins(50)
        this.layoutParams = layoutParams
        isAllCaps = false
        typeface = fontSelector.getTypeface()
    }

    fun getButton(): Button {
        return button
    }
}