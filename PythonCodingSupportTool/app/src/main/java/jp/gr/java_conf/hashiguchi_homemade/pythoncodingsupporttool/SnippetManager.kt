package jp.gr.java_conf.hashiguchi_homemade.pythoncodingsupporttool

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class SnippetManager(private val context: Context, private val addLayout: (String) -> Unit) {
    constructor(context: Context) : this(context, {})
    private var snippets: MutableList<String> = getSharedPreferences(context.getString(R.string.key)).toMutableList()
    private var snippetList: MutableList<Snippet> = mutableListOf()
    init {
        Log.i("SnippetManager", getSharedPreferences(context.getString(R.string.key)).toString())
        snippets.forEach {
            snippetList.add(Snippet(context, it))
        }
    }
    fun add(snippet: String) {
        snippetList.add(Snippet(context,snippet))
        snippets.add(snippet)
        addLayout(snippet)
        setSharedPreferences(context.getString(R.string.key), snippets)
        Log.i("SnippetManager:add", snippets.toString())
    }
    fun removeAt(index: Int) {
        snippetList.removeAt(index)
        snippets.removeAt(index)
        setSharedPreferences(context.getString(R.string.key), snippets)
        Log.i("SnippetManager:removeAt", snippets.toString())
    }

    fun getSnippetList(): List<Snippet> {
        snippets=getSharedPreferences(context.getString(R.string.key)).toMutableList()
        snippetList= mutableListOf()
        snippets.forEach {
            snippetList.add(Snippet(context, it))
        }
        val output = mutableListOf<String>()
        snippetList.forEach {
            output.add(it.code)
        }
        Log.i("SnippetManager:get", output.toString())
        return snippetList
    }
    fun getSnippetListAsStringList(): List<String> {
        val output = mutableListOf<String>()
        getSnippetList().forEach {
            output.add(it.code)
        }
        return output.toList()
    }
    private fun setSharedPreferences(key: String, snippets: List<String>) {
        val gson = Gson()
        val sharedPreferences = context.getSharedPreferences("snippets", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString(key, gson.toJson(snippets))
        editor.apply()
    }

    private fun getSharedPreferences(key: String): List<String> {
        val sharedPreferences = context.getSharedPreferences("snippets", Context.MODE_PRIVATE)
        val gson = Gson()
        val json = sharedPreferences.getString(key, null) ?: return listOf()
        return gson.fromJson(
            json,
            object : TypeToken<List<String>?>() {}.type
        )
    }
}