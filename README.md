# Simple coding support tools

PythonCodingSupportTools is a tool to assist in writing various programming codes, not only Python, on your smartphone.
Simply register a snippet and add that code with the push of a button.
Available for android smartphones.
PythonCodingSupportToolsは、Pythonに限らず、様々なプログラミングコードをスマートフォンで書くことを支援するツールです。
スニペットを登録するだけで、ボタンを押すだけでそのコードを追加することができます。
アンドロイドスマートフォンで利用可能です。

---

## Install/インストール

Please download the latest version of the apk file from the [release](https://gitlab.com/Kevin-Hashi/PythonCodingSupportTools/-/releases). After downloading, follow the guidance for each model and install.
最新版のapkファイルは[リリース](https://gitlab.com/Kevin-Hashi/PythonCodingSupportTools/-/releases)からダウンロードしてください。ダウンロード後、各機種のガイダンスに従い、インストールしてください。

---

## How to Use/使い方

A setup screen will appear upon initial startup.
You can register the fonts you wish to use in the spinner at the top of the screen, and snippets by clicking the Add button.
If you make a mistake, you can delete it with the trash button.
After adding the snippet, press the Back button to start writing code!
If you want to go back to the settings screen, you can use the "Setting" button in the upper right corner of the coding screen.
初回起動時に設定画面が表示されます。
画面上部のスピナーで使用したいフォント、追加ボタンでスニペットを登録できます。
間違えてしまった場合はごみ箱ボタンで削除できます。
追加したら戻るボタンを押してコードを書き始めましょう！
設定画面にまた行きたい場合はコーディング画面の右上の「Setting」ボタンからいけます。

---

## Requirements/必要要件

Models with Android 5.0 or later.
That's all there is to it.
Android 5.0以降を搭載したモデル
たったこれだけです。

---

## Fonts used/使用したフォント

- AnonymousPro-Regular <https://www.marksimonson.com/fonts/view/anonymous-pro>
- Cousine-Regular <https://fonts.google.com/specimen/Cousine>
- JetBrainsMono-VariableFont_wght <https://www.jetbrains.com/ja-jp/lp/mono/>
- OxygenMono-Regular <https://fonts.google.com/specimen/Oxygen+Mono>
- RobotoMono-Regular <https://fonts.google.com/specimen/Roboto+Mono>

---

## License/ライセンス

Copyright 2022 Kevin-Hashi

Licensed under the Apache License, Version 2.0 (the “License”);
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an “AS IS” BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

See the License for the specific language governing permissions and
limitations under the License.
